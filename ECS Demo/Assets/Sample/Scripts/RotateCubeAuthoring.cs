using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class RotateCubeAuthoring : MonoBehaviour, IConvertGameObjectToEntity
{
    public float rotationSpeed;
    
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddComponentData<RotationSpeed>(entity, new RotationSpeed
        {
            Value = rotationSpeed
        });
    }


    private void Update()
    {
        transform.rotation = math.mul(
            math.normalize(transform.rotation),
            quaternion.AxisAngle(math.up(), rotationSpeed * Time.deltaTime));
    }
}

public struct RotationSpeed : IComponentData
{
    public float Value;
}