using Unity.Entities;

[GenerateAuthoringComponent]
public struct AutoRotateCube : IComponentData
{
   public float Value;
}
