using System;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class CubeSpawnAuthoring : MonoBehaviour, IDeclareReferencedPrefabs, IConvertGameObjectToEntity
{
    public bool spawnEntities;
    public GameObject entityPrefab;
    public GameObject gameObjectPrefab;
    public int CountX;
    public int CountY;

    // Referenced prefabs have to be declared so that the conversion system knows about them ahead of time
    public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
    {
        referencedPrefabs.Add(entityPrefab);
    }

    // Lets you convert the editor data representation to the entity optimal runtime representation
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        if (spawnEntities)
        {
            var spawnerData = new Spawner
            {
                // The referenced prefab will be converted due to DeclareReferencedPrefabs.
                // So here we simply map the game object to an entity reference to that prefab.
                Prefab = conversionSystem.GetPrimaryEntity(entityPrefab),
                CountX = CountX,
                CountY = CountY
            };
            dstManager.AddComponentData(entity, spawnerData);
        }
    }

   
    private int spaceKeyPresses;
    private void Update()
    {
        if (!spawnEntities&& Input.GetKeyDown(KeyCode.Space))
        {
            for (var x = 0; x < CountX; x++)
            {
                for (var y = 0; y < CountY; y++)
                {
                    var instance = Instantiate(gameObjectPrefab);

                    // Place the instantiated in a grid with some noise
                    instance.transform.position = math.transform(transform.localToWorldMatrix,
                        new float3(x * 1.3F, noise.cnoise(new float2(x, y) * 0.21F) * 2 * spaceKeyPresses, y * 1.3F));
                }
            }
            spaceKeyPresses++;
        }
    }
}


public struct Spawner : IComponentData
{
    public int CountX;
    public int CountY;
    public Entity Prefab;
}